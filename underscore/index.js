export function log(o) {
  console.log(o) // eslint-disable-line no-console
  return o
}

export function map(o, fn) {
  let arr = []
  if(Array.isArray(o)) {
    for(let i=0; i< o.length; i++) {
      arr.push(fn(o[i], i))
    }
  }
  else { // regular js object
    arr = map(Object.keys(o), k => fn(o[k], k))
  }
  return arr

  // todo- es6 iterables?
}


export function range(n) {
  let arr = []
  for(let i=0; i<n; i++){
    arr.push(i)
  }
  return arr
}

export function each(o, fn) {
  if(Array.isArray(o)){
    for(let i=0; i<o.length; i++){
      fn(o[i])
    }
  }else{
    for (let key in Object.keys(o)) {
      fn(o.key)
    }
  }
}

export function reduce(o, fn) {

}

export function chain(o) {

}