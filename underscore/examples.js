import * as _ from './index'

_.log(_.map(_.range(200), x => x*x))
// [1, 4, 9, 16, ... ]

_.chain(_.range(200)).map(x => x*x).filter(x => x%2)
