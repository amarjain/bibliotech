/* global describe, it */
import expect from 'expect'

import * as _ from './index'

describe('underscore', () => {
  describe('collections', () => {
    it('_.each - array', () => {
      let sum = 0
      _.each([ 1, 2, 3 ], x => sum+= x)
      expect(sum).toEqual(6)
    })
  })
  describe('map', () => {
    it('array', () => {
      expect(_.map([ 1, 2, 3 ], x => x*x)).toEqual([ 1, 4, 9 ])
    })
  })
  describe('testing each', () => {
    it('object', () => {
      let sum = 0;
      _.each({ x: 1, b:2, c: 3}, x => sum += x)
      expect(sum).toEqual(6)
    })
  })
  describe('testing range', () => {
    it('array', () => {
      expect(_.range(4)).toEqual([0,1,2,3])
    })
  })
})
